package src;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class Server {

    private Server() throws RemoteException {
        Registry servicoNomes = LocateRegistry.createRegistry(2019);
        ServImpl serv = new ServImpl();
        servicoNomes.rebind("Transfers", serv);
    }

    public static void main(String[] args) {
        try {
            new Server();
            System.out.println("SERVER ATIVO");
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

}
