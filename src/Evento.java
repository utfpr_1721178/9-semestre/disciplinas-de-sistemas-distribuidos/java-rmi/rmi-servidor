package src;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

class Evento extends TimerTask {

    private final ArrayList<Cotacao> cotacoes;
    private final ArrayList<Transfer> transfers;
    private final ArrayList<Notificacao> notificacoes;
    private final ArrayList<Proposta> propostas;
    private final ArrayList<Proposta> reservas;

    Evento(){
        cotacoes = new ArrayList<>();
        transfers = new ArrayList<>();
        notificacoes = new ArrayList<>();
        propostas = new ArrayList<>();
        reservas = new ArrayList<>();
    }

    @Override
    public void run() {
        Date date = new Date();
        for (Cotacao cotacao : cotacoes) {
            if (!transferExiste(cotacao.getUuid())) {
                long diffInMillies = Math.abs(date.getTime() - cotacao.getDate().getTime());
                long diff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);
                if (diff <= 60000) {
                    notificaTransfer(cotacao, "Existem cotações que são compatíveis com o seu estilo de transfer, " +
                            "que tal fazer uma proposta com um preço melhor para o cliente " + cotacao.getUuid() + "?");
                }
            }
        }
    }

    private void notificaTransfer(Cotacao cotacao, String texto) {
        for (Transfer transfer : transfers){
            if (transfer.getVeiculo().equals(cotacao.getVeiculo())
            && Integer.parseInt(cotacao.getnPassageiros()) <= Integer.parseInt(transfer.getnPassageiros())
            && transfer.getCidade().equals(cotacao.getDestino())){
                try {
                    notificacoes.add(new Notificacao(texto, transfer, cotacao));
                    transfer.getInterfaceCli().echo(texto);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    void adicionaCotacao(Cotacao cotacao){
        cotacoes.add(cotacao);
        for (Transfer transfer : transfers) {
            propostas.add(new Proposta(cotacao, transfer));
        }

        try {
            cotacao.getInterfaceCli().echo("Cotação registrada com sucesso");
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }
    void adicionaTransfer(Transfer transfer){
        transfers.add(transfer);
        try {
            transfer.getInterfaceCli().echo("Transfer registrado com sucesso");
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }
    boolean cotacaoExiste(String uuid){
        for (Cotacao cotacao : cotacoes){
            if (cotacao.getUuid().equals(uuid)){
                return true;
            }
        }
        return false;
    }
    boolean transferExiste(String uuid){
        for (Transfer transfer : transfers){
            if (transfer.getUuid().equals(uuid)){
                return true;
            }
        }
        return false;
    }
    Transfer getTransfer(String uuid){
        for (Transfer transfer : transfers){
            if (transfer.getUuid().equals(uuid)){
                return transfer;
            }
        }
        return null;
    }

    void atualizaTransfer(Transfer transfer) {
        for (int i = 0; i < transfers.size(); i++){
            if (transfers.get(i).getUuid().equals(transfer.getUuid())){
                transfers.set(i, transfer);
                try {
                    transfer.getInterfaceCli().echo("Transfer atualizado com sucesso");
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public ArrayList<Cotacao> getCotacoes() {
        return cotacoes;
    }

    public ArrayList<Transfer> getTransfers() {
        return transfers;
    }

    public ArrayList<Notificacao> getNotificacoes() {
        return notificacoes;
    }

    public ArrayList<Proposta> getPropostas() {
        return propostas;
    }

    public ArrayList<Proposta> getReservas() {
        return reservas;
    }

    public void adicionaReserva(Proposta proposta) {
        reservas.add(proposta);
    }


    public void clienteSaiu(String uuid) {

    }

    void retirarTransfer(String uuid){
        for(Transfer transfer : transfers){
            if (transfer.getUuid().equals(uuid)){
                transfers.remove(transfer);
                break;
            }
        }
    }

    void retirarCotacao(String uuid){
        for(Cotacao cotacao : cotacoes){
            if (cotacao.getUuid().equals(uuid)){
                cotacoes.remove(cotacao);
                break;
            }
        }
    }
}
