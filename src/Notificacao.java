package src;

import java.io.Serializable;

class Notificacao implements Serializable {

    private String texto;
    private Transfer transfer;
    private Cotacao cotacao;
    //Este atributo serve para dizer, quando for 1, que a notificação é de cotacao para transfer.
    // E quando for 2 a notificação é transfer para cotação
    private int relacao;


    Notificacao(String texto, Transfer transfer, Cotacao cotacao){
        setTexto(texto);
        setTransfer(transfer);
        setCotacao(cotacao);
        setRelacao();
    }

    public int getRelacao() {
        return relacao;
    }

    private void setRelacao() {
        this.relacao = 2;
    }

    public String getTexto() {
        return texto;
    }

    private void setTexto(String texto) {
        this.texto = texto;
    }

    public Transfer getTransfer() {
        return transfer;
    }

    private void setTransfer(Transfer transfer) {
        this.transfer = transfer;
    }

    public Cotacao getCotacao() {
        return cotacao;
    }

    private void setCotacao(Cotacao cotacao) {
        this.cotacao = cotacao;
    }
}
