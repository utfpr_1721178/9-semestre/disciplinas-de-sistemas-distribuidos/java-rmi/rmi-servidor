package src;



import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Timer;

public class ServImpl extends UnicastRemoteObject implements InterfaceServ {

    private Evento evento;

    public ServImpl() throws RemoteException {
        super();
        evento = new Evento();
        new Timer().schedule(evento, 50000, 50000);
    }

    /**
     * Método que lista o menu de ações disponíveis no servidor e invoca o método
     * de impressão na tela do cliente através de referência do cliente.
     * @param interfaceCli referência do cliente
     */
    @Override
    public void menu(JavaRMICliente.src.InterfaceCli interfaceCli) {
        try {
            interfaceCli.menu("======================================"
            + "\n1 - Cotações"
            + "\n2 - Cadastro  e alteração de transfer"
            + "\n3 - Reservas"
            + "\n4 - Propostas"
            + "\n5 - Notificações"
            + "\n6 - Listar Transfers"
            + "\n7 - Sair"
            + "\n\n Escolha:");
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void menuOp(JavaRMICliente.src.InterfaceCli interfaceCli, String uuid, int op) throws RemoteException {
        switch (op) {
            case 1:
                interfaceCli.menuCotacao("=====================================" +
                    "\nPreencha os campos da Cotação" +
                    "\nExemplo: Origem, Destino, Data, Horário, Tipo de Veículo, Número de Passageiros, Preço Proposto"
                    +"\nExemplo: Curitiba, Orlando, 27/05/19, 15:00, Economia, 3, 1500"
                    + "\n Tipos de Veículos: Economia = até 3 pessoas, Conforto = até 3 pessoas, " +
                        "\nNegócio = até 3 pessoas, Prêmio = até 3 pessoas, VIP = até 3 pessoas, " +
                        "\nSUV = até 5 pessoas, Furgão = até 8 pessoas, Mini Ônibus = até 16 pessoas, " +
                        "\nÔnibus = até 50 pessoas, Helicóptero = até 5 pessoas");
                break;
            case 2:
                if (evento.transferExiste(uuid)){
                    //noinspection unused
                    Transfer transfer = evento.getTransfer(uuid);
                    interfaceCli.menuTransfer("Altere algum dos campos" +
                            "\nPara alterar veículo por exemplo, digite => Veículo:Economia"
                            + "\n Para Sair digite => :q", "ALTERACAO");
                } else {
                    interfaceCli.menuTransfer("Insira todos os campos no mesmo formato como o exemplo abaixo" +
                            "\nCampos: Cidade, Tipo do Veículo, Capacidade de Passageiros, Preço" +
                            "\nExemplo: Curitiba, VIP, 3, 1500","CADASTRO");
                }
                break;
            case 3:
                ArrayList<Proposta> reservas = evento.getReservas();
                for (Proposta reserva : reservas){
                    if (reserva.getTipo() == 1) {
                        if (reserva.getCotacao().getUuid().equals(uuid) || reserva.getTransfer().getUuid().equals(uuid)){
                            echoPropostaList(interfaceCli, reserva, reservas);
                        }
                    }
                    interfaceCli.menuPadrao("\n\nCancelar = c + indice da proposta (Exemplo: a5)" , 3);
                }
                break;
            case 4:
                ArrayList<Proposta> propostas = evento.getPropostas();
                for (Proposta proposta : propostas){
                    if (proposta.getTipo() == 2) {
                        if (proposta.getCotacao().getUuid().equals(uuid) || proposta.getTransfer().getUuid().equals(uuid)
                                && evento.transferExiste(uuid) && evento.cotacaoExiste(uuid)) {
                            echoPropostaList(interfaceCli, proposta, propostas);
                        } else if (proposta.getRelacao() == 2 && proposta.getCotacao().getUuid().equals(uuid)) {
                            echoPropostaList(interfaceCli, proposta, propostas);
                        }
                    }
                }
                interfaceCli.menuPadrao("\n\nAceitar = a + indice da proposta (Exemplo: a5)" +
                        "\nRejeitar = r+indice"
                        +"\nEditar = e+indice", 4);
                break;
            case 5:
                ArrayList<Notificacao> notificacoes = evento.getNotificacoes();
                for (Notificacao notificacao : notificacoes){
                    if (notificacao.getCotacao().getUuid().equals(uuid) || notificacao.getTransfer().getUuid().equals(uuid)){
                        interfaceCli.echo("\n" + notificacao.getTexto());
                    }
                }
                interfaceCli.menuNotificacao("\nPara sair digite :q");
                break;
            case 6:
                ArrayList<Transfer> transfers = evento.getTransfers();
                String filtro = interfaceCli.scannerListaTransfer("\nDigite a cidade destino");
                for (Transfer transfer : transfers){
                    if (transfer.getCidade().equals(filtro)){
                        interfaceCli.echo("\nTransfer: " + transfer.getUuid()
                        + "\n Cidade: " + transfer.getCidade()
                        + "\n Veículo: " + transfer.getVeiculo()
                        + "\n Capacidade: " + transfer.getnPassageiros()
                        + "\n Preço: " + transfer.getPreco());
                    }
                }
                interfaceCli.menuPadrao("\n\nReservar = rs + indice da proposta (Exemplo: rs5)", 4);
                break;
            case 7:
                evento.clienteSaiu(uuid);
                break;
        }
    }

    private void echoPropostaList(JavaRMICliente.src.InterfaceCli interfaceCli, Proposta proposta, ArrayList<Proposta> propostas) {
        try {
            interfaceCli.echo("\nIndice: " + propostas.indexOf(proposta)
                    + "\nCliente: " + proposta.getCotacao().getUuid()
                    + "\nTransfer: " + proposta.getTransfer().getUuid()
                    + "\nOrigem, Destino, Data e Horário, Tipo de Veículo, Número de Passageiros, Preço Proposto"
                    + "\n" + proposta.getCotacao().getOrigem()
                    + ", " + proposta.getCotacao().getDestino()
                    + ", " + proposta.getCotacao().getDate().toString()
                    + ", " + proposta.getCotacao().getVeiculo()
                    + ", " + proposta.getCotacao().getnPassageiros()
                    + ", " + proposta.getCotacao().getPrecoProposto());
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void adicionaCotacao(JavaRMICliente.src.InterfaceCli interfaceCli, String[] cotacao, String uuid) throws RemoteException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        try {
            Date date = simpleDateFormat.parse((cotacao[2] + " " + cotacao[3]));
            Cotacao cotacao1 = new Cotacao(interfaceCli, uuid.toLowerCase(), cotacao[0].toLowerCase(),
                    cotacao[1].toLowerCase(), date, cotacao[4].toLowerCase(), cotacao[5].toLowerCase(),
                    cotacao[6].toLowerCase());
            evento.adicionaCotacao(cotacao1);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void adicionaTransfer(JavaRMICliente.src.InterfaceCli interfaceCli, String[] transfer, String uuid) throws RemoteException {
        Transfer transfer1 = new Transfer(interfaceCli, uuid, transfer[0].toLowerCase(), transfer[1].toLowerCase(),
                transfer[2].toLowerCase());
        evento.adicionaTransfer(transfer1);
    }

    @Override
    public Transfer getTransfer(String uuid) throws RemoteException {
        return evento.getTransfer(uuid);
    }

    @Override
    public void atualizaTransfer(Transfer transfer) throws RemoteException {
        evento.atualizaTransfer(transfer);
    }

    @Override
    public void menuPadrao(String acao, int i) throws RemoteException {
        int index;
        if (acao.contains("a")){
            index = Integer.parseInt(acao.substring(1));
            if (i == 4){
                Proposta proposta = evento.getPropostas().get(index);
                proposta.setTipo(1);
                evento.adicionaReserva(proposta);
            }
        } else if (acao.contains("np")){
            index = Integer.parseInt(acao.substring(2));
        } else if (acao.contains("r")){
            index = Integer.parseInt(acao.substring(1));
        }
        else if (acao.contains("c")){
            index = Integer.parseInt(acao.substring(1));
        }
    }
}
