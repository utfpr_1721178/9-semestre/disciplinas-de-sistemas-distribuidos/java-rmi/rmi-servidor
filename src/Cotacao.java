package src;

import JavaRMICliente.src.InterfaceCli;

import java.io.Serializable;
import java.util.Date;

public class Cotacao implements Serializable {

    private String uuid;
    private String origem;
    private String destino;
    private Date date;
    private String veiculo;
    private String nPassageiros;
    private String precoProposto;
    private InterfaceCli interfaceCli;

    Cotacao(InterfaceCli interfaceCli, String uuid, String origem, String destino, Date date, String veiculo, String nPassageiros,
            String precoProposto){
        setInterfaceCli(interfaceCli);
        setUuid(uuid);
        setOrigem(origem);
        setDestino(destino);
        setDate(date);
        setVeiculo(veiculo);
        setnPassageiros(nPassageiros);
        setPrecoProposto(precoProposto);
    }

    public String getUuid() {
        return uuid;
    }

    private void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public InterfaceCli getInterfaceCli() {
        return interfaceCli;
    }

    private void setInterfaceCli(InterfaceCli interfaceCli) {
        this.interfaceCli = interfaceCli;
    }

    public String getOrigem() {
        return origem;
    }

    private void setOrigem(String origem) {
        this.origem = origem;
    }

    public String getDestino() {
        return destino;
    }

    private void setDestino(String destino) {
        this.destino = destino;
    }

    public Date getDate() {
        return date;
    }

    private void setDate(Date date) {
        this.date = date;
    }

    public String getVeiculo() {
        return veiculo;
    }

    private void setVeiculo(String veiculo) {
        this.veiculo = veiculo;
    }

    public String getnPassageiros() {
        return nPassageiros;
    }

    private void setnPassageiros(String nPassageiros) {
        this.nPassageiros = nPassageiros;
    }

    public String getPrecoProposto() {
        return precoProposto;
    }

    private void setPrecoProposto(String precoProposto) {
        this.precoProposto = precoProposto;
    }
}
