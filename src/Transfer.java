package src;

import JavaRMICliente.src.InterfaceCli;

import java.io.Serializable;

public class Transfer implements Serializable {

    private String uuid;
    private String cidade;
    private String veiculo;
    private String nPassageiros;
    private String preco;
    private InterfaceCli interfaceCli;

    Transfer(InterfaceCli interfaceCli, String uuid, String veiculo, String nPassageiros, String preco){
        setInterfaceCli(interfaceCli);
        setUuid(uuid);
        setVeiculo(veiculo);
        setnPassageiros(nPassageiros);
        setPreco(preco);
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getUuid() {
        return uuid;
    }

    private void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getVeiculo() {
        return veiculo;
    }

    void setVeiculo(String veiculo) {
        this.veiculo = veiculo;
    }

    public String getnPassageiros() {
        return nPassageiros;
    }

    void setnPassageiros(String nPassageiros) {
        this.nPassageiros = nPassageiros;
    }

    public String getPreco() {
        return preco;
    }

    void setPreco(String preco) {
        this.preco = preco;
    }

    public InterfaceCli getInterfaceCli() {
        return interfaceCli;
    }

    private void setInterfaceCli(InterfaceCli interfaceCli) {
        this.interfaceCli = interfaceCli;
    }
}
