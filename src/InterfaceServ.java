package src;



import java.rmi.Remote;
import java.rmi.RemoteException;

public interface InterfaceServ extends Remote {

    void menu(JavaRMICliente.src.InterfaceCli interfaceCli) throws RemoteException;

    void menuOp(JavaRMICliente.src.InterfaceCli interfaceCli, String uuid, int op) throws RemoteException;

    void adicionaCotacao(JavaRMICliente.src.InterfaceCli interfaceCli, String[] cotacao, String uuid) throws RemoteException;

    void adicionaTransfer(JavaRMICliente.src.InterfaceCli interfaceCli, String[] transfer, String uuid) throws RemoteException;

    Transfer getTransfer(String uuid) throws RemoteException;

    void atualizaTransfer(Transfer transfer) throws RemoteException;

    void menuPadrao(String acao, int i) throws RemoteException;
}
