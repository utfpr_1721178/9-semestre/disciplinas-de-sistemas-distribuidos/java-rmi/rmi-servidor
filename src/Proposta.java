package src;

import java.io.Serializable;

class Proposta implements Serializable {

    private Cotacao cotacao;
    private Transfer transfer;
    //Atributo que define se a proposta está firmada entre as duas partes, pendente ou rejeitada.
    //0 = rejeitada, 1 = firmada, 2 = pendente
    private int tipo;
    //Este atributo serve para dizer, quando for 1, que a notificação é de cotacao para transfer.
    // E quando for 2 a notificação é transfer para cotação
    private int relacao;
    private String precoAcordado;

    Proposta(Cotacao cotacao, Transfer transfer){
        setCotacao(cotacao);
        setTransfer(transfer);
        setTipo(2);
        setRelacao();
    }

    public String getPrecoAcordado() {
        return precoAcordado;
    }

    public void setPrecoAcordado(String precoAcordado) {
        this.precoAcordado = precoAcordado;
    }

    public int getRelacao() {
        return relacao;
    }

    private void setRelacao() {
        this.relacao = 2;
    }

    public Cotacao getCotacao() {
        return cotacao;
    }

    private void setCotacao(Cotacao cotacao) {
        this.cotacao = cotacao;
    }

    public Transfer getTransfer() {
        return transfer;
    }

    private void setTransfer(Transfer transfer) {
        this.transfer = transfer;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }
}
